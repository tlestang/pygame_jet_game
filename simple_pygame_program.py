# Simple pygame program from
# https://realpython.com/pygame-a-primer

# Import and initialize the pygame library
import random
import pygame
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

SCREEN_HEIGHT = 600
SCREEN_WIDTH = 800


class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.image.load("assets/jetfighter.png").convert()
        self.rect = self.surf.get_rect()

    def update(self, pressed_keys):
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -5)
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 5)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-5, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(5, 0)

        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT


class Ennemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Ennemy, self).__init__()
        self.surf = pygame.Surface((20, 10))
        self.surf.fill((255, 255, 255))
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speed = random.randint(5, 20)

    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()


pygame.init()

# Set up the drawing window
clock = pygame.time.Clock()
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
all_sprites = pygame.sprite.Group()
ennemies = pygame.sprite.Group()
player = Player()
all_sprites.add(player)

# Create new ennemy event
ADDENNEMY = pygame.USEREVENT + 1
pygame.time.set_timer(ADDENNEMY, 250)

# Run until the user asks to quit
running = True
while running:
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
        elif event.type == QUIT:
            running = False
        elif event.type == ADDENNEMY:
            new_ennemy = Ennemy()
            ennemies.add(new_ennemy)
            all_sprites.add(new_ennemy)

    pressed_keys = pygame.key.get_pressed()
    player.update(pressed_keys)
    ennemies.update()

    screen.fill((0, 0, 0))
    for entity in all_sprites:
        screen.blit(entity.surf, entity.rect)

    if pygame.sprite.spritecollideany(player, ennemies):
        player.kill()
        running = False

    pygame.display.flip()

    # Ensure program maintains a rate of 30 frames per second
    clock.tick(30)


# Done! Time to quit.
pygame.quit()
